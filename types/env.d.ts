declare namespace NodeJS {
  interface ProcessEnv {
    /**
     * 应用标题
     */
    APP_TITLE: string
  }
}
