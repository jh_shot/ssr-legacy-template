# SSR Legacy Template

## 简介

SSR Legacy Template 是一个开源的服务端渲染模版。

## 特性

- **技术栈**：使用 Vue2/NuxtBridge 等前端前沿技术开发
- **TypeScript**：应用程序级 JavaScript 的语言
- **特性**：使用 Vue 和 TSX 编写组件

## 准备

- [Node](http://nodejs.org/) 和 [Git](https://git-scm.com/)
- [Vue2](https://v2.cn.vuejs.org/v2/guide/)
- [NuxtBridge](https://nuxt.com/docs/bridge/overview)

## 安装使用

- 获取项目代码

```bash
git clone https://gitee.com/jh_shot/ssr-legacy-template.git
```

- 安装依赖

```bash
pnpm i
```

- 运行

```bash
pnpm dev
```

- 打包

```bash
pnpm build
```

## 注意事项

- 装上 volar 插件后更好的支持模板开发
- 升级 Nuxt 版本需要先删除 node_modules 和依赖锁定文件
- 不要使用 Vite，ElementUI 组件需要用 `client-only` 包裹才能使用，[issue](https://github.com/nuxt/bridge/issues/348)
- Nuxt Bridge 下 `useAsyncData` 和 `useFetch` 不可用，使用 `$fetch`
- Nuxt Bridge 下 `$fetch` 需自己指定返回类型
- 当前不支持 `"type": "module"`，[issue](https://github.com/nuxt/bridge/issues/1144)
- Nuxt Bridge 下 pinia 的 `storesDirs` 无效，使用 `imports` 代替

## 待解决
