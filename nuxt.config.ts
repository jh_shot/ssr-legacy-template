import { defineNuxtConfig } from '@nuxt/bridge'
import type { Options as PostCSSNestedOptions } from 'postcss-nested'
import { ElementUiResolver } from 'unplugin-vue-components/resolvers'

export default defineNuxtConfig({
  srcDir: 'src/',
  app: {
    head: {
      link: [{ rel: 'icon', type: 'image/svg+xml', href: '/vue.svg' }],
      title: process.env.APP_TITLE
    }
  },
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '@/assets/styles/element-dark.css',
    '@/assets/styles/theme.css',
    '@/assets/styles/main.css'
  ],
  postcss: {
    plugins: {
      'postcss-nested': { preserveEmpty: true } as PostCSSNestedOptions
    }
  },
  plugins: [
    {
      src: '@/plugins/element-ui.ts',
      mode: 'client'
    },
    {
      src: '@/plugins/error-handler.ts',
      mode: 'all'
    },
    {
      src: '@/plugins/fetch.ts',
      mode: 'all'
    },
    {
      src: '@/plugins/icons.ts',
      mode: 'all'
    }
  ],
  modules: [
    '@nuxtjs/color-mode',
    '@pinia/nuxt',
    '@vueuse/nuxt',
    [
      'unplugin-vue-components/nuxt',
      {
        dts: false,
        dirs: [],
        resolvers: [
          ElementUiResolver({
            importStyle: false
          })
        ]
      }
    ],
    'vue-types-nuxt'
  ],
  colorMode: {
    classSuffix: ''
  },
  pinia: {
    disableVuex: true
  },
  imports: {
    dirs: ['apis', 'stores']
  },
  components: true,
  bridge: {
    meta: true,
    vite: false,
    nitro: true,
    capi: { legacy: false },
    typescript: { esbuild: true }
  },
  runtimeConfig: {
    public: {
      title: process.env.APP_TITLE
    }
  }
})
