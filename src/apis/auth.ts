export const authApi = {
  login: (body: any) =>
    $fetch<string>('/api/auth/login', { method: 'post', body })
}
