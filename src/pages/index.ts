// 保留该文件作为首页的入口
import IndexPage from './index/index.vue'

export default defineComponent({
  render() {
    return h(IndexPage)
  }
})
