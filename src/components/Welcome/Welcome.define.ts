import { string } from 'vue-types'

export const welcomeProps = () => ({
  /**
   * 欢迎消息
   */
  msg: string().isRequired
})

// 不加默认导出 nuxt bridge 会报错
export default {}
