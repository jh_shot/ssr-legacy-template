import { Message } from 'element-ui'

export default defineNuxtPlugin(nuxtApp => {
  globalThis.$fetch = $fetch.create({
    onRequest({ options }) {},
    onResponseError({ response }) {
      const { statusCode, message } = response._data

      Message.error({
        message: `【${statusCode}】${message}`,
        duration: 1500
      })
    }
  })
})
