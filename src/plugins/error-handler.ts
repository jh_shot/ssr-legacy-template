import Vue from 'vue'

export default defineNuxtPlugin(nuxtApp => {
  Vue.config.errorHandler = (err, instance, info) => {
    console.error(err, instance, info)
  }
})
