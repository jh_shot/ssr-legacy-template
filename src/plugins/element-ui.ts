import type { ElementUIOptions } from 'element-ui'
import Vue from 'vue'

export default defineNuxtPlugin(nuxtApp => {
  Vue.prototype.$ELEMENT = { size: 'small' } as ElementUIOptions
})
