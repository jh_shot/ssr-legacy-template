import type { IconifyIcons } from '@iconify/types'
import { addCollection, loadIcons } from '@iconify/vue2'

const files = require.context('~/assets/icons/json', false, /\.json$/)

export default defineNuxtPlugin(() => {
  const icons = {} as IconifyIcons
  const iconNames = [] as string[]

  for (const key of files.keys()) {
    const icon = files(key)

    icons[icon.key] = icon.value
    iconNames.push(`@local:custom:${icon.key}`)
  }

  addCollection({ prefix: 'custom', icons }, 'local')
  loadIcons(iconNames)
})
