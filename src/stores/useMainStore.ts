export const useMainStore = defineStore('main', () => {
  const state = reactive({
    count: 0,
    token: useCookie('token')
  })

  /**
   * 加法
   */
  function increment() {
    state.count++
  }

  /**
   * 登录
   */
  async function handleLogin(payload: any) {
    const data = await authApi.login(payload)

    state.token = data
  }

  /**
   * 退出
   */
  async function handleLogout() {
    // 必须重新执行 useCookie，否则刷新后回到登录页无法设置 cookie
    const token = useCookie('token')
    token.value = undefined

    state.token = undefined
  }

  return {
    ...toRefs(state),
    increment,
    handleLogin,
    handleLogout
  }
})
