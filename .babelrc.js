/**
 * @type {import('@babel/core').TransformOptions}
 */
module.exports = {
  compact: 'auto'
}
